﻿using BusinessObjects.Objects.Models;
using BusinessRules.Rules.Interfaces;
using Dapper;
using DataAccess.Queries.Implementations;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Rules.Implementations
{
	public class GameRules : IGameRules
	{
		readonly string connectionstring = @"Server=127.0.0.1;Database=LaserTagDB;Uid=root;SslMode=None";
		readonly GameQueries queries = new GameQueries();
		readonly IGameRules gameRules;

		GameRules(IGameRules _gameRules) => gameRules = _gameRules;

		public GameRules() { }

		public async Task<Game> CreateGame(Game game)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Game>(queries.CreateGame(game.Name, game.GameType))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}
		public async Task<Game> DeleteGame(Game game)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Game>(queries.DeleteGameById(game.Id))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}
		public async Task<Game> GetGameById(int id)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Game>(queries.GetGameById(id))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}
		public async Task<IEnumerable<Game>> GetGames()
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return await db.QueryAsync<Game>(queries.GetGames());
				}
			}
			catch (Exception ex) { throw ex; }
		}
		public async Task<Game> UpdateGame(Game game)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Game>(queries.UpdateGameById(game.Id, game.Name, game.GameType))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}
	}
}
