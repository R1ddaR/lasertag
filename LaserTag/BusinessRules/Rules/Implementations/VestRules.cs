﻿using BusinessObjects.Objects.Models;
using BusinessRules.Rules.Interfaces;
using Dapper;
using DataAccess.Queries;
using DataAccess.Queries.Implementations;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Rules.Implementations
{
	public class VestRules : IVestRules
	{
		readonly string connectionstring = @"Server=127.0.0.1;Database=LaserTagDB;Uid=root;SslMode=None";
		readonly VestQueries queries = new VestQueries();
		readonly IVestRules vestRules;

		public VestRules() { }

		public VestRules(IVestRules _vestRules) => vestRules = _vestRules;

		public async Task<Vest> CreateVest(Vest vest)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Vest>(queries.CreateVest(vest.PlayerId, vest.GameId))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}

		public async Task<Vest> DeleteVest(Vest vest)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Vest>(queries.CreateVest(vest.PlayerId, vest.GameId))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}

		public async Task<Vest> GetVestById(int id)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Vest>(queries.GetVestById(id))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}

		public async Task<IEnumerable<Vest>> GetVests()
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return await db.QueryAsync<Vest>(queries.GetVests());
				}
			}
			catch (Exception ex) { throw ex; }
		}

		public async Task<Vest> UpdateVest(Vest vest)
		{
			try
			{
				using (IDbConnection db = new MySqlConnection(connectionstring))
				{
					return (await db.QueryAsync<Vest>(queries.UpdateVestById(vest.Id, vest.PlayerId, vest.GameId))).FirstOrDefault();
				}
			}
			catch (Exception ex) { throw ex; }
		}
	}
}
