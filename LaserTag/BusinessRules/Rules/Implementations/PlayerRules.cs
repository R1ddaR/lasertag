﻿using BusinessObjects.Objects.Models;
using BusinessRules.Rules.Interfaces;
using Dapper;
using DataAccess.Queries.Implementations;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Rules.Implementations
{
    public class PlayerRules : IPlayerRules
    {
        readonly string connectionstring = @"Server=127.0.0.1;Database=LaserTagDB;Uid=root;SslMode=None";
        readonly PlayerQueries playerQueries = new PlayerQueries();
        readonly IPlayerRules playerRules;

        public PlayerRules() { }

        public PlayerRules(IPlayerRules _playerRules) => playerRules = _playerRules;

        public async Task<IEnumerable<Player>> GetPlayers()
        {
            try
            {
                using (IDbConnection db = new MySqlConnection(connectionstring))
                {
                    return await db.QueryAsync<Player>(playerQueries.GetPlayers);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Player> GetPlayerById(int id)
        {
            try
            {
                using (IDbConnection db = new MySqlConnection(connectionstring))
                    return await db.QueryFirstAsync<Player>(playerQueries.GetPlayerById(id));
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Player> CreatePlayer(Player player)
        {
            try
            {
                using (IDbConnection db = new MySqlConnection(connectionstring))
                    return await db.QueryFirstAsync<Player>(playerQueries.CreatePlayer(player.Username, player.Password));
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Player> UpdatePlayer(Player player)
        {
            try
            {
                using (IDbConnection db = new MySqlConnection(connectionstring))
                    return await db.QueryFirstAsync<Player>(playerQueries.UpdatePlayer(player.Id, player.Username, player.Password));
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Player> DeletePlayer(Player player)
        {
            try
            {
                using (IDbConnection db = new MySqlConnection(connectionstring))
                {
                    await db.QueryFirstAsync<Player>(playerQueries.DeletePlayerById(player.Id));
                    return player;
                }

            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Player> GetPlayerByUserName(string username)
        {
            try
            {
                using (IDbConnection db = new MySqlConnection(connectionstring))
                    return (await db.QueryAsync<Player>(playerQueries.GetPlayerByUsername(username))).FirstOrDefault();
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
