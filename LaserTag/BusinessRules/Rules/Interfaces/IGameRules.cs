﻿using BusinessObjects.Objects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Rules.Interfaces
{
	interface IGameRules
	{
		Task<IEnumerable<Game>> GetGames();
		Task<Game> GetGameById(int id);
		Task<Game> CreateGame(Game game);
		Task<Game> UpdateGame(Game game);
		Task<Game> DeleteGame(Game game);
	}
}
