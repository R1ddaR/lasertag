﻿using BusinessObjects.Objects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Rules.Interfaces
{
    public interface IPlayerRules
    {
        Task<IEnumerable<Player>> GetPlayers();
        Task<Player> GetPlayerById(int id);
        Task<Player> CreatePlayer(Player player);
        Task<Player> UpdatePlayer(Player player);
        Task<Player> DeletePlayer(Player player);
        Task<Player> GetPlayerByUserName(string username);
    }
}
