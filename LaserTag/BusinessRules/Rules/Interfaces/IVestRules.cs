﻿using BusinessObjects.Objects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Rules.Interfaces
{
	public interface IVestRules
	{
		Task<IEnumerable<Vest>> GetVests();
		Task<Vest> GetVestById(int id);
		Task<Vest> CreateVest(Vest vest);
		Task<Vest> UpdateVest(Vest vest);
		Task<Vest> DeleteVest(Vest vest);
	}
}
