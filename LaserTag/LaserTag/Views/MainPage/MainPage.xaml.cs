﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace LaserTag.Views
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Back(object sender, RoutedEventArgs e) => Frame.GoBack();

        private void Setting(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Account));
        }

		private void CreateGame(object sender, RoutedEventArgs e)
		{
			Frame.Navigate(typeof(CreateGame));
		}

		private void JoinGame(object sender, RoutedEventArgs e)
		{
			Frame.Navigate(typeof(JoinGame));
		}

        private void SkirmishGame(object sender, RoutedEventArgs e)
        {

        }

        private void Builds(object sender, RoutedEventArgs e)
        {

        }
    }
}
