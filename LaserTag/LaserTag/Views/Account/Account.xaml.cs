﻿using System;
using System.Collections.Generic;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace LaserTag.Views
{
    public sealed partial class Account : Page
    {
        public Account()
        {
            this.InitializeComponent();
        }

        private void Back(object sender, RoutedEventArgs e) => Frame.GoBack();
    }
}
