﻿using BusinessRules.Rules.Implementations;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace LaserTag.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Login : Page
    {
        PlayerRules playerRules = new PlayerRules();

        public Login()
        {
            this.InitializeComponent();
        }

        private void Submit(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(username.Text) && !string.IsNullOrEmpty(password.Password))
            {
                var player = playerRules.GetPlayerByUserName(username.Text).Result;
				if (player != null)
					if (player.Username.Equals(username.Text) && player.Password.Equals(password.Password))
						Frame.Navigate(typeof(MainPage));
					else
					{
						username.Background = new SolidColorBrush(Colors.Red);
						password.Background = new SolidColorBrush(Colors.Red);
					}		
			}
        }

        private void Quit(object sender, RoutedEventArgs e) => CoreApplication.Exit();

		private void Back(object sender, RoutedEventArgs e) => CoreApplication.Exit();

		private void SingUp(object sender, RoutedEventArgs e)
		{

		}
	}
}
