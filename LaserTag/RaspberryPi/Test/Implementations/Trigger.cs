﻿using Microsoft.IoT.Lightning.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices;
using Windows.Devices.Gpio;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace RaspberryPi.Test.Implementations
{
    public class Trigger
    {
        private const int trigger_pin = 5;
        private GpioPin triggerPin;
        //private bool pressed;

        public Trigger()
        {
            if (LightningProvider.IsLightningEnabled)
                LowLevelDevicesController.DefaultProvider = LightningProvider.GetAggregateProvider();

            var gpio = GpioController.GetDefault();

            if (gpio == null)
                return;

            triggerPin = gpio.OpenPin(trigger_pin);

            if (triggerPin.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                triggerPin.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                triggerPin.SetDriveMode(GpioPinDriveMode.Input);

            triggerPin.DebounceTimeout = TimeSpan.FromMilliseconds(50);
        }
    }
}
