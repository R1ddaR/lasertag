﻿using Microsoft.IoT.Lightning.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Devices.Pwm;
using Windows.System.Threading;

namespace RaspberryPi.Test.Implementations
{
    class ServoMotor : IBackgroundTask
    {
        ThreadPoolTimer timer;
        double clockwisePulseLength = 1;
        double counterClockwisePulseLegnth = 2;
        double restingPulseLegnth = 0;
        double currentPulseLength = 0;
        double secondPulseLength = 0;
        int iteration = 0;
        PwmPin motorPin;
        PwmController pwmController;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            if (!LightningProvider.IsLightningEnabled)
                return;

            var deferral = taskInstance.GetDeferral();

            pwmController = (await PwmController.GetControllersAsync(LightningPwmProvider.GetPwmProvider()))[0];
            motorPin = pwmController.OpenPin(0);

            pwmController.SetDesiredFrequency(50);
            motorPin.SetActiveDutyCyclePercentage(restingPulseLegnth);
            motorPin.Start();


            timer = ThreadPoolTimer.CreatePeriodicTimer(Timer_Tick, TimeSpan.FromMilliseconds(500));
        }

        private void Timer_Tick(ThreadPoolTimer timer)
        {
            iteration++;
            if (iteration % 3 == 0)
            {
                currentPulseLength = clockwisePulseLength;
                secondPulseLength = counterClockwisePulseLegnth;
            }
            else if (iteration % 3 == 1)
            {
                currentPulseLength = counterClockwisePulseLegnth;
                secondPulseLength = clockwisePulseLength;
            }
            else
            {
                currentPulseLength = 0;
                secondPulseLength = 0;
            }

            double desiredPercentage = currentPulseLength / (1000.0 / pwmController.ActualFrequency);
            motorPin.SetActiveDutyCyclePercentage(desiredPercentage);
        }
    }
}
