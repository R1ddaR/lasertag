﻿using Microsoft.IoT.Lightning.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;

namespace RaspberryPi.Test.Implementations
{
    public class Blink
    {
        private const int led_pin = 5;
        private GpioPin pin;
        private GpioPinValue pinValue;
        public Blink()
        {
            var gpio = GpioController.GetDefault();
            if (gpio == null)
                return;

            pin = gpio.OpenPin(led_pin);
            pinValue = GpioPinValue.High;
            pin.Write(pinValue);
            pin.SetDriveMode(GpioPinDriveMode.Output);
        }   
    }
}
