﻿using Microsoft.IoT.Lightning.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices;
using Windows.Devices.Gpio;
using Windows.Devices.Pwm;

namespace RaspberryPi.Test.Implementations
{
    public class BlinkPWM
    {
        private const int led_pin = 5;

        public BlinkPWM()
        {
            InitGpio();
        }

        public async void InitGpio()
        {
            if (!LightningProvider.IsLightningEnabled)
                return;

            LowLevelDevicesController.DefaultProvider = LightningProvider.GetAggregateProvider();

            var pwmControllers = await PwmController.GetControllersAsync(LightningPwmProvider.GetPwmProvider());
            var pwmController = pwmControllers[1];
            pwmController.SetDesiredFrequency(50);

            var pin = pwmController.OpenPin(led_pin);
            pin.SetActiveDutyCyclePercentage(.25);
            pin.Start();
        }
    }
}
