﻿using BusinessObjects.Objects.Models;
using BusinessRules.Rules.Implementations;
using BusinessRules.Rules.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest.mockups.BusinessRules
{
    [TestClass]
    public class PlayerRules_Test
    {
        [TestMethod]
        public void GetPlayersNotNull()
        {
            List<Player> players = new List<Player>
            {
                new Player { Id = 1, Username = "test", Password = "test" },
                new Player { Id = 2, Username = "blah", Password = "test" }
            };
            Mock<IPlayerRules> mockRepository = new Mock<IPlayerRules>();
            mockRepository.Setup(x => x.GetPlayers()).ReturnsAsync(players);

            PlayerRules playerRules = new PlayerRules(mockRepository.Object);
            Assert.IsNotNull(playerRules.GetPlayers());
        }

        [TestMethod]
        public void GetPlayerByIdNotNull()
        {
            Player player = new Player() { Id = 1, Username = "test", Password = "test" };
            Mock<IPlayerRules> mockRepository = new Mock<IPlayerRules>();
            mockRepository.Setup(x => x.GetPlayerById(player.Id)).ReturnsAsync(player);

            PlayerRules playerRules = new PlayerRules(mockRepository.Object);
            Assert.IsNotNull(playerRules.GetPlayerById(player.Id));
        }

        [TestMethod]
        public void CreatePlayerNotNull()
        {
            Player player = new Player() { Id = 1, Username = "test", Password = "test" };
            Mock<IPlayerRules> mockRepository = new Mock<IPlayerRules>();
            mockRepository.Setup(x => x.CreatePlayer(player)).ReturnsAsync(player);

            PlayerRules playerRules = new PlayerRules(mockRepository.Object);
            Assert.IsNotNull(playerRules.CreatePlayer(player));
        }

        [TestMethod]
        public void UpdatePlayerNotNull()
        {
            Player player = new Player() { Id = 1, Username = "test", Password = "test" };
            Mock<IPlayerRules> mockRepository = new Mock<IPlayerRules>();
            mockRepository.Setup(x => x.UpdatePlayer(player)).ReturnsAsync(player);

            PlayerRules playerRules = new PlayerRules(mockRepository.Object);
            Assert.IsNotNull(playerRules.UpdatePlayer(player));
        }
    }
}
