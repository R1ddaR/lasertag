﻿using DataAccess.Queries.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Queries
{
	public class VestQueries : IVestQueries
	{
		public string CreateVest(int playerId, int gameId) => $"Insert INTO Vests (PlayerId, GameId) Values ({playerId},{gameId})";
		public string DeleteVestById(int id) => $"DELETE FROM Vests WHERE Id={id}";
		public string GetVestById(int id) => $"SELECT * FROM Vests WHERE Id={id}";
		public string GetVests() => @"SELECT * FROM Vests";
		public string UpdateVestById(int id, int playerId, int gameId) => $"UPDATE Vests SET PlayerId={playerId}, GameId={gameId} WHERE Id={id}";
	}
}

