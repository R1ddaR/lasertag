﻿using DataAccess.Queries.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Queries.Implementations
{
    public class PlayerQueries : IPlayerQueries
    {
        public PlayerQueries() { }
        public string GetPlayers => @"SELECT * FROM Players";
        public string GetPlayerById(int id) => $"SELECT * FROM Players WHERE Id={id}";
        public string CreatePlayer(string username, string password) => $"INSERT INTO Players (username, password) VALUES({username}, {password})";
        public string UpdatePlayer(int id, string username, string password) => $"UPDATE Player SET username='{username}',password='{password}' WHERE Id={id}";
        public string DeletePlayerById(int id) => $"DELETE FROM Players WHERE Id={id}";
        public string GetPlayerByUsername(string username) => $"SELECT * FROM Players WHERE Username='{username}'";
    }
}
