﻿using DataAccess.Queries.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Queries.Implementations
{
	public class GameQueries : IGameQueries
	{
		public string CreateGame(string name, string gameType) => $"Insert INTO Games (Name, gameType) Values ({name},{gameType})";
		public string DeleteGameById(int id) => $"DELETE FROM Vests WHERE Id={id}";
		public string GetGameById(int id) => $"DELETE FROM Games WHERE Id={id}";
		public string GetGames() => @"SELECT * FROM Games";
		public string UpdateGameById(int id, string name, string gameType) => $"UPDATE Vests SET Name={name}, GameType={gameType} WHERE Id={id}";
	}
}
