﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Queries.Interfaces
{
	interface IVestQueries
	{
		string GetVests();
		string GetVestById(int id);
		string CreateVest(int playerId, int GameId);
		string UpdateVestById(int id, int playerId, int GameId);
		string DeleteVestById(int id);
	}
}
