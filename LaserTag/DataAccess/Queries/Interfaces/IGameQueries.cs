﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Queries.Interfaces
{
	interface IGameQueries
	{
		string GetGames();
		string GetGameById(int id);
		string CreateGame(string name, string gameType);
		string UpdateGameById(int id, string name, string gameType);
		string DeleteGameById(int id);
	}
}
