﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Queries.Interfaces
{
    interface IPlayerQueries
    {
        string GetPlayers { get; }
        string GetPlayerById(int id);
        string CreatePlayer(string username, string password);
        string UpdatePlayer(int id, string username, string password);
        string DeletePlayerById(int id);
    }
}
